package com.controller;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.UserInfoDAO;
import com.domain.UserInfo;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String account = request.getParameter("account");
		String pswd = request.getParameter("pswd");
		String role = request.getParameter("role");
		UserInfo userInfo = new UserInfoDAO().login(account, pswd, role);
		if (userInfo.getId() != null) {
			HttpSession session = request.getSession();
			session.setMaxInactiveInterval(3 * 60 * 60);// 设置session有效期为三小时
			session.setAttribute("userInfo", userInfo);
			response.sendRedirect(request.getServletContext().getContextPath()
					+ "/view/main.jsp");
		} else {
			request.setAttribute("rs", "用户名或密码不正确，请重新登录。");
			this.getServletContext().getRequestDispatcher("/index.jsp")
			.forward(request, response);
		}
	}

}
