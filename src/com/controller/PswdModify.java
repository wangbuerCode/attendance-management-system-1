package com.controller;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.DbUtil;

@WebServlet("/view/PswdModify")
public class PswdModify extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String account = request.getParameter("account");
		String pswd = request.getParameter("pswd");
		String sql = "update t_user_info set pswd = '" + pswd + "' where account = '" + account + "'";
		DbUtil.add_modify(sql);
		request.setAttribute("rs", "修改成功，下次请以新密码登陆");
		request.getRequestDispatcher("/view/pswd_modify.jsp").forward(request, response);
	}
}
