
package com.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.DbUtil;

@WebServlet("/view/ClazzJoinServlet")
public class ClazzJoinServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		if ("add".equals(method)) {
			String name = request.getParameter("name");
			String cname = request.getParameter("cname");
			String duetime = request.getParameter("duetime");
			String num1 = request.getParameter("num1");
			String num2 = request.getParameter("num2");
			String num3 = request.getParameter("num3");
			String sql = "insert into t_clazz_join(name, cname, duetime, num1, num2, num3) values('"
			+ name + "', '" + cname + "', '" + duetime + "', '" + num1 + "', '" + num2 + "', '" + num3 + "')";
			DbUtil.add_modify(sql);
		} else if ("del".equals(method)) {
			String[] id = request.getParameterValues("selectFlag");
			DbUtil.del("t_clazz_join", id);
		}
		request.getRequestDispatcher("/view/clazz_join_manage.jsp").forward(request, response);
	}
}
