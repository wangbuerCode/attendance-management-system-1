package com.controller;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.DbUtil;

@WebServlet("/view/CourseServlet")
public class CourseServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		if ("add".equals(method)) {
			String name = request.getParameter("name");
			String score = request.getParameter("score");
			String hour = request.getParameter("hour");
			String tid = request.getParameter("tid");
			String sql = "insert into t_course(name, score, hour, tid) values('"
			+ name + "', '" + score + "','" + hour + "', '" + tid + "')";
			DbUtil.add_modify(sql);
		} 
		request.getRequestDispatcher("/view/course_manage.jsp").forward(request, response);
	}
}
