package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserInfoDAO;

@WebServlet("/UserInfoIsExistServlet")
public class UserInfoIsExistServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// 阻止缓存
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-store"); // HTTP1.1
		response.setHeader("Pragma", "no-cache"); // HTTP1.0
		response.setDateHeader("Expires", 0); // prevents catching at proxy
												// server

		PrintWriter out = response.getWriter();
		
		//js传过来的汉字进行转码，避免汉字的时候会出现乱码
		String id = request.getParameter("id");
		boolean b = new UserInfoDAO().accountIsExist(id);
		if (b) {
			out.print("用户名已存在，请重新输入。");
		}
	}

}
