package com.controller;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.DbUtil;

@WebServlet("/view/StuJoinServlet")
public class StuJoinServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		if ("add".equals(method)) {
			String account = request.getParameter("account");
			String cid = request.getParameter("cid");
			String duetime = request.getParameter("duetime");
			String due = request.getParameter("due");
			String sql = "insert into t_stu_join(account, cid, duetime, due) values('"
			+ account + "', '" + cid + "', '" + duetime + "', '" + due + "')";
			DbUtil.add_modify(sql);
		}
		request.getRequestDispatcher("/view/stu_join_manage.jsp").forward(request, response);
	}
}
