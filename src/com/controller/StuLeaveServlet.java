package com.controller;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.DbUtil;
import com.util.SessionUtil;

@WebServlet("/view/StuLeaveServlet")
public class StuLeaveServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		if ("add".equals(method)) {
			String account = SessionUtil.getUserInfo(request, response).getAccount();
			String starttime = request.getParameter("starttime");
			String reason = request.getParameter("reason");
			String num = request.getParameter("num");
			String sql = "insert into t_stu_leave(account, pubtime, starttime, reason, num, status) values('"
			+ account + "', now(),'" + starttime + "', '" + reason + "', '" + num + "', '�����')";
			DbUtil.add_modify(sql);
		}else if ("modify".equals(method)) {
			int id = Integer.parseInt(request.getParameter("id"));
			String duetime = request.getParameter("duetime");
			String duename = SessionUtil.getUserInfo(request, response).getAccount();
			String result = request.getParameter("result");
			String sql = "update t_stu_leave set duetime = '" + duetime + "', duename = '" + duename + "', result = '" + result + "', status = '�����' where id = " + id;
			DbUtil.add_modify(sql);
		}
		request.getRequestDispatcher("/view/stu_leave_manage.jsp").forward(request, response);
	}
}
