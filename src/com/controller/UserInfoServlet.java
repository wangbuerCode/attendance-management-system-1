package com.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.DbUtil;

@WebServlet("/view/UserInfoServlet")
public class UserInfoServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("method");
		if ("add".equals(method)) {
			String account = request.getParameter("account");
			String pswd = request.getParameter("pswd");
			String role = request.getParameter("role");
			String name = request.getParameter("name");
			String sex = request.getParameter("sex");
			String title = request.getParameter("title");
			String sql = "insert into t_user_info(account, pswd, role, name, sex, title) values('"
			+ account + "', '" + pswd + "','" + role + "', '" + name + "', '" + sex + "', '" + title + "')";
			DbUtil.add_modify(sql);
			
			
			if(role.equals("学生")){
				String sql_stu = "insert into t_student(account) values('" + account + "')";
				DbUtil.add_modify(sql_stu);
			}else if(role.equals("任课老师")){
				String sql_teacher = "insert into t_teacher(account) values('" + account + "')";
				DbUtil.add_modify(sql_teacher);
			}
			
		} else if ("del".equals(method)) {
			String[] id = request.getParameterValues("selectFlag");
			DbUtil.del("t_user_info", id);
		} else if ("modify".equals(method)) {
			int id = Integer.parseInt(request.getParameter("id"));
			String pswd = request.getParameter("pswd");
			String role = request.getParameter("role");
			String name = request.getParameter("name");
			String sex = request.getParameter("sex");
			String title = request.getParameter("title");
			String sql = "update t_user_info set pswd = '" + pswd + "', role = '" + role + "', name = '" + name + "', sex = '" + sex + "', title = '" + title + "' where id = " + id;
			DbUtil.add_modify(sql);
		}
		request.getRequestDispatcher("/view/user_info_manage.jsp").forward(request, response);
	}
}
