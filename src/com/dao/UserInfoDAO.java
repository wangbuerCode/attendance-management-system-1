package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.domain.UserInfo;
import com.util.DbUtil;

public class UserInfoDAO {
	
	public UserInfo login(String account, String pswd, String role) {
		UserInfo userInfo = new UserInfo();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "select * from t_user_info where account = ? and pswd = ? and role = ?";
		try {
			conn = DbUtil.getConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, account);
			stmt.setString(2, pswd);
			stmt.setString(3, role);
			rs = stmt.executeQuery();
			if (rs.next()) {
				userInfo.setId(rs.getString(1));
				userInfo.setAccount(rs.getString(2));
				userInfo.setPswd(rs.getString(3));
				userInfo.setRole(rs.getString(4));
				userInfo.setName(rs.getString(5));
				userInfo.setSex(rs.getString(6));
				userInfo.setTitle(rs.getString(7));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(rs);
			DbUtil.close(stmt);
			DbUtil.close(conn);
		}
		return userInfo;
	}
	
	public boolean accountIsExist(String account) {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from t_user_info where account = ?";
		try {
			conn = DbUtil.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, account);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(rs);
			DbUtil.close(pstmt);
			DbUtil.close(conn);
		}
		return flag;
	}

}
