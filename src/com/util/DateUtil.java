package com.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 日期及时间处理函数
 * 
 * @author Roman
 */
public class DateUtil {

	private static final String DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 获取默认格式的当前日期及时间
	 * 
	 * @return 日期及时间的字符串
	 */
	public static String getDateTime() {
		Date currDate = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_PATTERN);
		String dateTime = sdf.format(currDate);
		return dateTime;
	}
	
	/**
	 * 获取pattern传入格式的当前日期及时间
	 * 
	 * @param pattern
	 * 
	 * @return 日期及时间的字符串
	 */
	public static String getDateTime(String pattern) {
		Date currDate = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String dateTime = sdf.format(currDate);
		return dateTime;
	}

	/**
	 * 获取星期几
	 * 
	 * @return 返回星期字符串
	 */
	public static String getWeek() {
		Calendar calendar = Calendar.getInstance();
		String[] weeks = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		int index = calendar.get(Calendar.DAY_OF_WEEK);
		String week = weeks[index - 1];
		return week;
	}

	/**
	 * 将date转换成默认时间格式的字符串
	 * 
	 * @param date
	 * 
	 * @return String
	 */
	public static String date2Str(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_PATTERN);
		return sdf.format(date);
	}

	/**
	 * 将date转换成pattern格式的字符串
	 * 
	 * @param date
	 * @param pattern
	 * 
	 * @return String
	 */
	public static String date2Str(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}

	/**
	 * 将字符串类型转换为时间类型
	 * 
	 * @param str
	 * 
	 * @return Date
	 */
	public static Date str2Date(String str) {
		Date date = null;
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_PATTERN);
		try {
			date = sdf.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 将字符串按照pattern格式转换为时间类型
	 * 
	 * @param str
	 * @param pattern
	 * 
	 * @return Date
	 */
	public static Date str2Date(String str, String pattern) {
		Date date = null;
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			date = sdf.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 将时间格式化
	 * 
	 * @param date
	 * 
	 * @return Date
	 */
	public static Date DatePattern(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_PATTERN);
		try {
			String dd = sdf.format(date);
			date = str2Date(dd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 将时间按照pattern格式格式化
	 * 
	 * @param date
	 * @param pattern
	 * 
	 * @return Date
	 */
	public static Date DatePattern(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			String dd = sdf.format(date);
			date = str2Date(dd, pattern);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 获取传入日期的前interval天的日期(interval<0) or 获取传入日期的后interval天的日期(interval>0)
	 * 
	 * @param date
	 * @param interval
	 * 
	 * @return Date
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static Date getDateByInterval(Date date, int interval) {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		calendar.setTime(date);

		calendar.add(calendar.DATE, interval);

		return str2Date(date2Str(calendar.getTime()));
	}

	/**
	 * 计算两个日期相隔的天数
	 * 
	 * @param starttime
	 * @param endtime
	 * 
	 * @return int
	 */
	public static int nDaysBetweenTwoDate(String starttime, String endtime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date firstDate = null;
		Date secondDate = null;
		try {
			firstDate = df.parse(starttime);
			secondDate = df.parse(endtime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int nDay = (int) ((secondDate.getTime() - firstDate.getTime()) / (24 * 60 * 60 * 1000));
		return nDay;
	}

	/**
	 * 比较传入的字符串日期跟当前系统日期的关系
	 * 
	 * @param str
	 * 
	 * @return int(n>0 在当前日期之后，n<0 在当前日期之前，n=0等于当前日期。)
	 */
	public static int compareTo(String str) {
		int isAfterOrBeforeDate = str2Date(str).compareTo(str2Date(getDateTime()));
		return isAfterOrBeforeDate;
	}

}